# Outcomes


In addition to this documentation site, the outcomes of this Collabathon include the following artifacts. Each of these are living documents, and we welcome feedback and will provide space for community review as we further develop each of these scopes of work. 

 
## Summary Report
**[FAIR Tech Registry Collabathon Report](https://gitlab.com/OpenTEAMAg/codesign/fair-registry/-/raw/d6f3a2274feda6c44fe5e2bff07300bab9f92b58/docs/assets/FAIR%20Tech%20Registry%20Collabathon%20Report.pdf)**

> This is our initial comprehensive report documenting the FAIR Tech Registry Collabathon, including an overview of work, background, goals, process, outcomes, and conclusions. 

## Criteria Evaluation
**[FAIR Tech Registry: Criteria Evaluation](https://docs.google.com/document/d/1eZPjasHcEXFLkHnbH0s4mpm6J6kLNPcbR0aT6N-ul0Q/edit#heading=h.677snh5sm0b8)**

> This document is a brief summary of the collabathon work focused around critera. It covers process, development of categories, and minimal and maximal lists of initial proposed criteria for entries in a developer-facing registry.



## Governance Evaluation
**[FAIR Tech Registry: Governance Evaluation](https://docs.google.com/document/d/1gvCQw-i0ufWWdwJ1HclMR2xITdJ-pQi0JNiFo91jdIM/edit#heading=h.rz6zy4wftf6m)**

> This document is a summary of our collabathon work focused on governance processes, as well as our initial recommendations for governance and roles for a developer-focused tech registry. 

### Document Review

**[FAIR Tech Registry Review Session](https://lu.ma/fair-tech-registry-review)**

The first full review session of these Collabathon outputs is scheduled for February 29th, 2:00-3:00pm EST. 

​Our intention for this session is to gather feedback, questions, and additional resources as we begen to develop a proof of concept for a developer-facing FAIR Tech Ecosystem Registry. 


## Ongoing Work

Our immediate next phase of work involves applying the community-developed evaluation criteria to use cases already in development in the OpenTEAM network as a proof of concept. If you are interested in supporting this work, please contact vic@openteam.community. 


The methods of working together that we established in this collaborative session led directly to other productive collaborations, including a recent [Partnerships for Climate-Smart Commodities Event Storming](https://openteamag.gitlab.io/codesign/pcsc/) in December. As we develop our practical and conceptual toolkit for collaboration, this series of meetings served to ground future work with the persistent reminder that value multiplies via sharing, and that we’re building interconnecting pieces that depend on each other. We look forward to hearing from you as we build into the future. 

