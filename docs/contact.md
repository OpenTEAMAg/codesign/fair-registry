# Contact Us

If you have any questions or feedback about these documents and this Collabathon, or are interested in contributing to this work, please contact [vic@openteam.community](mailto:vic@openteam.community).

For more information about OpenTEAM, you can find us at [our website](https://openteam.community/) or email [info@openteam.community](mailto:info@openteam.community).

Thank you for your interest, and we look forward to hearing from you. 
 

