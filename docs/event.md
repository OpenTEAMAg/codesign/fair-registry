# Event

![Sticky notes on window](https://gitlab.com/OpenTEAMAg/codesign/fair-registry/-/raw/master/docs/assets/window-stickies2.jpg?ref_type=heads)

## Collabathon Summary

This collabathon consisted of eight meetings, occurring bi-weekly on Tuesdays at 8am PT/11am ET
from September 12th - December 19th, 2023.

[**You can see notes and co-design from all sessions here.**](https://miro.com/app/board/uXjVMq-cvco=/)

Through this Collabathon process, we examined:

* The relevant roles and personas in our community
* How a registry or registries would have to function to meet their needs 
* Models for registry entry, approval, and moderation
* Required criteria for listing tools or modules
* Governance possibilties for registry entries individually and collectively


We came to new realizations about the challenges and requirements for stewarding a developer-focused registry, and produced a proposal to develop in practice. 

As a result of this Collabathon, we have the clarity needed to test these outputs against an initial use case, a road map for next steps, and a more comprehensive understanding of how this registry and its component parts will support and enable other aspects of our collective technological ecosystem.  

## Overview of Sessions

![Overview of Sessions](https://gitlab.com/OpenTEAMAg/codesign/fair-registry/-/raw/38b210e4e8115776865c9392fa73a39c5cf51a78/docs/assets/sessions.png)

Screenshot (modified) from FAIR Tech Registry Collabathon Miro board, 12/19 - “Collabathon Recap"



- *Session 1* - Presented an introduction to FAIR & CARE standards, discussed FAIR in practice in the context of agriculture and tech, and shared an overall vision for registry and key questions of this convening.

- *Session 2*  - Covered the concept of modularity of tools, and showed us we needed more time to think through personas as a group. 

- *Session 3* - Assessed registry needs for three personas (farmers, researchers, and developers), and showed that we need to narrow our scope and define constraints of registry based on a use case with immediate needs & applicability.

-  *Session 4* - Focused on the use case of developers with a short-term pathway for engagement, and discussed how to represent modular tech components and support a community of practice in a registry context.

- *Session 5* - Synthesized the group responses we received, met with participants to think through process and structure for registry entry by criteria, and populated a document with draft examples building off the categories we identified (Link to document) 

- *Session 6* - focused on what the process of accepting entries into this registry might look like, and shared thoughts on multiple potential models for governance (Link to document) & developed a drafted governance model to further explore

- *Session 7* - Explored roles necessary for governance under the drafted model proposed in the previous session. We evaluated how a two-tiered review model could work and what the implications were for assessment. 

- *Session 8* - The final session of this collabathon. Recap of the last seven sessions as a whole, with reflection on process, discussion and interest assessments for future work. 


## Followup Events

### Human-Centered Design working group

Presentation to OpenTEAM
*January 31, 2024*

- [Recording of Session (Zoom)](https://us02web.zoom.us/rec/play/Q8caY0fbxW4jzdZyZl34hgbYX2WqT3u3Vv2KKZyzijQAOYxzqCBeCKigRTYACwFjpTdGG_wWlkBc51S_.Or8Yjp1UpjgO9Diy?canPlayFromShare=true&from=share_recording_detail&continueMode=true&componentName=rec-play&originRequestUrl=https%3A%2F%2Fus02web.zoom.us%2Frec%2Fshare%2FZz4VH1SXXxtBgrsMwAVZao2OB6tlXLln7_7vSatvZjV8kk94xsjuboWdHLpFl39M.XE5WvAenATGpES-w)
- [Session Notes](https://docs.google.com/document/d/1z1YLwrZVPPmC9MPBjFye2HmbucCvt-5ZbwqSc7TLh2o/edit)

### Community Review Session

We will hold our first update and review session on February 29, 2024, from 2:00pm-3:00pm EST. 

**[Link to Register](https://lu.ma/fair-tech-registry-review)**


​Our intention for this session is to gather feedback, questions, and additional resources as we head into the next phase of work, focused on developing a proof of concept for a developer-facing FAIR Tech Ecosystem Registry. 


