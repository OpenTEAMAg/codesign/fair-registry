# Process

![Vintage IBM template](https://gitlab.com/OpenTEAMAg/codesign/fair-registry/-/raw/master/docs/assets/HIPO%20template.jpg?ref_type=heads)
*Vintage IBM template stencil - Via [archive.org](https://archive.org/details/IBMHIPOTemplate)*

## Background
This collabathon developed as part of an ongoing collaboration between [OpenTEAM](https://openteam.community/) and [partner organizations](https://openteamag.gitlab.io/codesign/fair-registry/participants/) in our ecosystem. This is a continuation of groundwork established in our [Ag Data Use Agreements](https://openteam-agreements.community/) collabathon, and work with the [11th Hour Project](https://11thhourproject.org/ag-tech/). 

We see this registry as a critical piece of infrastructure for Ag Data Wallet development, and will be listed and list the forthcoming ‘connector’ meta-registry of identity-specific content. 

![Connector](https://gitlab.com/OpenTEAMAg/codesign/fair-registry/-/raw/7871f1dbba4192950b235b555c788e02904ad166/docs/assets/adw-registry-connector.png)

This is also our first collabathon after OpenTEAM's 2023 tech reframing, and the first with a new Tech/Collaborative Projects team - so this project involved simultaneously creating both new language for what we were building; and ways of working together in a new context through experimentation . 

## Goals

Our goal for this registry is to create a structure for creators of relevant tooling to share what they’re doing, illustrate how it is relevant, and provide avenues for others to build on individual components while maintaining appropriate privacy and ownership. 

Our prototype of a FAIR registry is focused on communicating a concept of sharable **modules** or **components**, with a structured, replicable way of **documenting** what a module is and does, and **how it can be applied** in a new instance. As Dr. Ankita Raturi described the proposed registry, it is meant to inventory “Not my shiny thing, but the building blocks of what makes my thing shiny” 

By designing around modular units, categorized by their relevance to different personas, and cataloged based on practical requirements for use, we see this registry as an instantiation of FAIR principles, a way to put our aspirations into practice.  


### Kickoff statement

*September 12, 2023*


This Collabathon will engage stakeholders to collaboratively explore the design of a FAIR Tech Ecosystem Registry for Agriculture, which is a crucial next step for achieving greater interoperability across the ag tech ecosystem. FAIR principles (Finable, Accessible, Interoperable, and Reusable) aim to strengthen the capacity of individuals, organizations, and technological systems to easily find, access, work collaboratively, and reuse data to steward technological innovation. We aim to put these principles in practice, while protecting user-generated data and strengthening data privacy.

In addition to gaining a general understanding and consensus on the technical and user experience concept, the fall sessions will focus on refining the acceptance and review process that will be required to maintain and update the registry.

This Collabathon will focus on the following primary requirements:

* Scope priorities for a registry, i.e. what types of toolkits, applications, utilities, data, libraries, and models to initially include
* Determine the acceptance governance process
* Define initial technical, ethical/CARE (Collective Benefit, Authority to Control, Responsibility, Ethics), and legal/license standards and criteria for listing and review

In prioritizing this project, OpenTEAM has identified parallels between how the agricultural technology space is currently developing and the role curated registries like the Google Play Store, Apple Store and Ubuntu Software and related developer toolkits played in accelerating the development of interoperable mobile and desktop applications.


## Methods
This collabathon consisted of eight meetings, occurring bi-weekly on Tuesdays at 8am PT/11am ET from September 12th - December 19th, 2023. 

Preparation (framework development, research, invitations and outreach) for this event began in the summer of 2023, although due to the collaborative and interconnected nature of work in this community, many of the concepts addressed here have roots in earlier projects. 

* OpenTEAM fellow Sara Legg contributed to work focused on needs for a persona-based registry in 2022-2023
* This personas work built on [Common Profile](https://www.our-sci.net/the-common-profile/) project work before that. 
* The questions this collabathon addresses emerged from the discovery process for the [Ag Data Use Agreements project](https://openteam-agreements.community/), which ran in the OpenTEAM Tech Working Group from March to June of 2023. 


This Collabathon convened subject matter experts with interest and expertise in methodology assessment, mission-driven tech development, interoperability and informatics, and other specialties. We invited participants from our network of collaborators in specific roles, and also extended open invitations to our community broadly.

 Sessions averaged fourteen participants, with 88% percent of listed participants attending more than one session, and 50% of listed participants attending five or more sessions. 

The majority of interaction happened through a combination of video calls and mapping in Miro, which was both a place to work collaboratively in sessions and a form of persistent documentation. 

> **[The Miro board of all sessions in this Collabathon can be found here.](https://miro.com/app/board/uXjVMq-cvco=/)** 

 We also used collaborative working documents via Google Docs, individual video interviews, and email conversations to collect feedback. Contact lists and event outreach were managed using Luma, which also provided an additional method for submitting feedback. A typical session would involve an introduction and orientation to the project as a whole, synthesis and discussion of a specific topic, an activity and/or breakout groups. Sessions lasted one hour. 


## Results Statement

*February 16th, 2024*

Through this Collabathon process, we examined the relevant roles and personas present in our community, and how a registry or registries that we’re envisioning would have to function to meet their diverse needs. We explored multiple models for registry entry, approval, and moderation, and developed a preliminary set of required criteria for listing tools or modules. We came to new realizations about the challenges and requirements for hosting and stewarding version one of a developer-focused registry, and produced a proposal for governance to develop in practice. As a result, we have the clarity needed to test these outputs against an initial use case, a road map for next steps, and a more comprehensive understanding of how this registry and its component parts will support and enable other aspects of our collective technological ecosystem.  


## Summary and documentation

After the Collabathon sessions concluded on December 19th, OpenTEAM staff began synthesis of outputs. An internal team continued to meet twice weekly to coordinate ongoing work, including presenting this collabathon to the Human-Cenetered Design working group for feedback, and preparing for community review. 

We held a community review session for outputs of this collabathon on February 29th, 2024 from 2:00-3:00pm EST. 

**[FAIR Tech Registry Review Session](https://lu.ma/fair-tech-registry-review)**

In addition to this page, the outcomes of this Collabathon include the following artifacts. Each of these are living documents, and we welcome feedback and will provide space for community review as we further develop each of these scopes of work. 


- [FAIR Tech Registry Collabathon Report](https://gitlab.com/OpenTEAMAg/codesign/fair-registry/-/raw/d6f3a2274feda6c44fe5e2bff07300bab9f92b58/docs/assets/FAIR%20Tech%20Registry%20Collabathon%20Report.pdf)

- [FAIR Tech Registry Criteria Evaluation](https://docs.google.com/document/d/1eZPjasHcEXFLkHnbH0s4mpm6J6kLNPcbR0aT6N-ul0Q/edit#heading=h.677snh5sm0b8)

- [FAIR Tech Registry Governance Evaluation ](https://docs.google.com/document/d/1gvCQw-i0ufWWdwJ1HclMR2xITdJ-pQi0JNiFo91jdIM/edit#heading=h.rz6zy4wftf6m)


**Additional Documents**

- [FAIR Tech Registry Collabathon - Miro](https://miro.com/app/board/uXjVMq-cvco=/)
- [FAIR & CARE Foundations notes](https://docs.google.com/document/d/1OOvYqhlGEnNqRkngz5mypTLiXdIdXIBdXg6ROOLYk9M/edit)
- [Community resource list - FAIR](https://docs.google.com/document/d/1t6fy1WVuFx3Js6iAxmHouF-_PaXVYiLUmi1-g1_t7kM/edit#heading=h.bljsil42np8k)
- [Community Resources - OS Licenses](https://docs.google.com/document/d/1Bx1pK4M2NhuYaHAprATqLmBwzzBJDhUrA7U21hnllwQ/edit#heading=h.bljsil42np8k)



