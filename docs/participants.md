# Participating Orgs



## Who was invovled 

- [OpenTEAM](https://openteam.community/)
- [Hylo](https://www.hylo.com/) & [Terran Collective](https://www.terran.io/)
- [Our-Sci](https://www.our-sci.net/)
- [Purdue University AxI Lab](http://aginformaticslab.org/)
- [Tech Matters](https://techmatters.org/)

- [LiteFarm](https://www.litefarm.org/) & [University of British Columbia](https://ubcfarm.ubc.ca/)
- [Socialroots](https://www.socialroots.io/)
- [Regen network](https://www.regen.network/)
- [IC-Foods](https://www.ic-foods.org/)
- [Digital Gaia](https://www.digitalgaia.earth/)
- [Carbon A-List](https://carbonalist.com/)
- [Afresh Technologies](https://www.afresh.com/)
- [Global Regeneration Co-Lab](https://www.grc.earth/)
- [Conservation Technology Information Center](https://www.grc.earth/)
- [PUR.Co](https://www.pur.co/)
- UW-Madison [ICICLE](https://icicle.osu.edu/)
- [Point Blue Conservation Science](https://www.pointblue.org/)
- Individual contributors



Thank you so much for your generous and thoughtful contributions, and for committing to the work of co-creation. 


[You can find out more about OpenTEAM partnerships at our website.](https://openteam.community/who-we-are/)