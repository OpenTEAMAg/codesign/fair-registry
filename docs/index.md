
![FAIR icons](https://gitlab.com/OpenTEAMAg/codesign/fair-registry/-/raw/master/docs/assets/FAIR-icons.png?ref_type=heads)

# About


This [Collabathon](https://openteam.community/how-we-work/), which ran from September 2023 to Spring of 2024, explored the design of a FAIR Tech Ecosystem Registry for Agriculture as a next step for achieving interoperability across the ag tech ecosystem. Our goal is to develop a curated ecosystem that enables the practice of FAIR principles, while making it easier for groups to innovate and develop new projects.

Key design objectives in our discussion included custom application of frameworks, enabling building in open-source tech, and ensuring data compatibility.

This project developed as part of an ongoing collaboration between [OpenTEAM](https://openteam.community/) and [partner organizations](https://openteamag.gitlab.io/codesign/fair-registry/participants/) in our ecosystem. This is a continuation of groundwork established in our [Ag Data Use Agreements](https://openteam-agreements.community/) collabathon, and work with the [11th Hour Project](https://11thhourproject.org/ag-tech/). This page and the documents we link were co-created as a shared record of the collaborative process.

* This page covers the work that happened in the discovery and co-design stage specific to this Collabathon. 
* Next, we will start testing & designing prototype registry pieces - we will be documenting that work [here](https://openteamag.gitlab.io/fair-tech-registry/devs/faq/) as it develops. 


## What do we mean by a FAIR Tech Ecosystem Registry? 

A curated list of tools and components that comply with FAIR principles, with built-in support structures & requirements for describing what each entry is for, how it interacts with other tools and data, and how it can be built on top of or repurposed.

This includes the creation of social, legal, technical, and operational frameworks that make each of these statements meaningful and enforceable in practice.

With this registry concept, we are working to create a structure for developers in the open ag tech space to share what they’re doing, illustrate its relevance, and provide avenues for others to build off of individual components in transparent ways. This will make it easier for developers to innovate and develop new projects and technologies that benefit farmers.


## What do we mean by FAIR, and FAIR in Practice? 

The Fair principles were published in 2016 in a paper called "[The FAIR Guiding Principles for scientific data management and stewardship](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4792175/)" with the goal of 'improv[ing] the infrastructure supporting the reuse of scholarly data.'

The four foundational principles are Findability, Accessibility, Interoperability, and Reusability. These each have a series of technical sub-principles which you can find [here via Go-FAIR](https://www.go-fair.org/fair-principles/). While the vast majority of literature and organization around the FAIR principles is focused primarily on machine findability for research data, this is not the only area where these concepts are meaningful. The [original paper](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4792175/) itself emphasizes this: 

> "Importantly, it is our intent that the principles apply not only to ‘data’ in the conventional sense, but also to the algorithms, tools, and workflows that led to that data. All scholarly digital research objects —from data to analytical pipelines—benefit from application of these principles, since all components of the research process must be available to ensure transparency, reproducibility, and reusability. " 


In order for us to enact the FAIR principles in our community of practice, we are focused on building scaffolding and forms of communication to make these concepts broadly comprehensible for multiple actors across the open, agriculture, and tech realms. This includes building reference implementations to test concepts against real user needs, attending to process documentation, designing methods for sharing information across contexts, developing and deploying new schemas & ontologies, generating community buy-in through collaborative experimentation, and more. 

## What else is important for this project? 

While this session was focused primarily on understanding and implementing the FAIR principles, we feel the need to continually affirm that our work in this sphere is simultaneously and equally invested in creating solutions centered around [data rights](https://openteam-agreements.community/billofrights/), [data justice](https://journals.sagepub.com/doi/10.1177/2053951717736335), and congruence with [CARE principles](https://www.gida-global.org/care). 

In order to solve these interconnected problems, we need to approach them from multiple directions & build in parallel.  This is just one part of a series of collabathons, each focused on discrete pieces necessary for a functional, open ag tech ecosystem. Forthcoming workstreams include: 

* [Ag Data Wallet UX Collabathon](https://openteamag.gitlab.io/agricultural-data-wallet/)
* [OpenTEAM Marketplace Working Group](https://openteamag.gitlab.io/codesign/markets/)
* Consent Management and Data Use Agreements
* Identity Services, Hosting, and Security
* ...and more, based on outputs & scopes developed within these codesign processes. 
