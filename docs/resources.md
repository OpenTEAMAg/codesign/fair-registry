# Resources


This is a community-compiled list of resources relevant to FAIR tech and its application to agricultural data. It was assembled as part of the 2023 FAIR Tech Ecosystem Registry Collabathon, and was last updated February 14, 2024. 


## OpenTEAM resources 


- [OpenTEAM Equity Collabathon Resource Guide](https://openteam-equity-resource.webflow.io/foundations/equity)
- [OpenTEAM Ag Data Use Agreements](https://openteam-agreements.community/) 
- [Interoperability & Data Sovereignty - One-page resource (pdf)](https://drive.google.com/file/d/1f5MS32uqQG2piW-1Y2Js393VdjTX-Pwe/view)
- [FAIR & CARE Principles - Collabathon notes](https://docs.google.com/document/d/1OOvYqhlGEnNqRkngz5mypTLiXdIdXIBdXg6ROOLYk9M/edit)
- [BC Agricultural Climate Action Research Network - Ethical Data Governance](https://www.bcacarn.ca/projects-2/ethical-data-governance/)
     - [Video Presentation by Greg Austic & Dorn Cox](https://www.youtube.com/watch?v=Mva9c83FWlw&list=TLGGt5s_fpQQCeoyMDA5MjAyMw&t=747s) - Community-Led Data Governance and Farmer-Controlled Data


## FAIR Principles 

**Findability, Accessibility, Interoperability, Reusability**

### Overview

- [The FAIR Guiding Principles for scientific data management and stewardship](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4792175/) - Wilkinson et al
This is the original paper outlining these principles, published in SciData in 2016 

- [FAIR Principles: Interpretations and Implementation Considerations](https://direct.mit.edu/dint/article/2/1-2/10/10017/FAIR-Principles-Interpretations-and-Implementation) Jacobsen et al, 2020. Provides structure & designation between the four foundational principles and 15 guiding principles



### Implementation

- [How to Make Your Data FAIR](https://howtofair.dk/how-to-fair/) - User-friendly description of details in FAIRification process, including Documentation, Metadata, Persistent identifiers, licenses & more. 

- [FAIR Cookbook](https://faircookbook.elixir-europe.org/content/recipes/introduction/FAIR-cookbook-audience.html) - Community-driven resource of ‘recipes’ for combining the FAIR principles with examples from Life Science disciplines. 

- [How To GO-FAIR](https://www.go-fair.org/how-to-go-fair/) - Resource from GO-FAIR consortium on implementing FAIR principles

- [Cultivating FAIR Principles for Agri-Food Data](https://www.sciencedirect.com/science/article/pii/S0168169922002265#:~:text=The%20FAIR%2Dprinciples%20have%20been,principles%20in%20agriculture%20and%20food.) - Jan Top , Sander Janssen et al Computers and Electronics in Agriculture 196 (2022) 

- [Cloudy, increasingly FAIR; revisiting the FAIR Data guiding principles for the European Open Science Cloud](https://content.iospress.com/articles/information-services-and-use/isu824) - Outlines FAIR implementation as a spectrum of possibilities, “how data can become increasingly FAIR digital objects,” & what FAIR is and is not. 

- from [Turning FAIR into Reality](https://op.europa.eu/en/publication-detail/-/publication/7769a148-f1f6-11e8-9982-01aa75ed71a1/language-en)
    1. Central to the realisation of FAIR are FAIR Digital Objects, which may represent data, software, or other research resources [...] 
    2. FAIR Digital Objects can only exist in a FAIR ecosystem, comprising key data services that are needed to support FAIR. [...]
    3. Interoperability frameworks that define community practices for data sharing, data formats, metadata standards, tools and infrastructure play a fundamental role. [...]
    4. FAIR must work for humans and for machines.

- [FAIRPoints events series](https://www.fairpoints.org/)

- [ResearchSpace FAIR Data Podcast](https://www.researchspace.com/fair-data-podcast)



### Case Studies 

- [Sharing Begins at Home: How Continuous and Ubiquitous FAIRness Can Enhance Research Productivity and Data Reuse](https://hdsr.mitpress.mit.edu/pub/qjpg8oik/release/2) - by William P Dempsey, Ian Foster, Scott Fraser, and Carl Kesselman

- [Center for Expanded Data Annotation and Retrieval (CEDAR)](https://metadatacenter.org/): Tool for templatizing biomedical metadata using JSON schema 

- [Harvard Dataverse FAIR Principles for Geospatial Data](https://github.com/IQSS/dataverse-pm/issues/22)

- [Ocean FAIR Data Services](https://www.frontiersin.org/articles/10.3389/fmars.2019.00440/full)



## CARE Principles  

**Collective Benefit, Authority to Control, Responsibility, Ethics**

### Overview


- [CARE Principles for Indigenous Data Governance](https://www.gida-global.org/care) -  Homepage for resources, hosted by the Global Indigenous Data Alliance 

- [Operationalizing the CARE and FAIR Principles for Indigenous data futures](https://www.nature.com/articles/s41597-021-00892-0)


- [Be FAIR and CARE presentation](https://www.youtube.com/watch?v=AYaJLcckNCY) By Dr. Lydia Jennings, University of Arizona


<br>

### Indigenous Data & Governance

- [Research Data Alliance Indigenous Data Sovereignty interest group](https://rd-alliance.org/groups/international-indigenous-data-sovereignty-ig)

- [University of Toronto Indigenous Studies Resource Guide - Indigenous Data Sovereignty](https://guides.library.utoronto.ca/indigenousstudies/datasovereignty)

- [INDIGENOUS DATA SOVEREIGNTY: TOWARD AN AGENDA](https://fnigc.ca/wp-content/uploads/2020/09/bbe195ddc231e3b1222d71ca4c09ae62_indigenous_data_sovereignty_toward_an_agenda_11_2016.pdf) - PDF, Edited by Tahu Kukutai and John Taylor 

- [First Nations Information Governance Center](https://fnigc.ca/what-we-do/ocap-and-information-governance/) - OCAP & Information Governance training course

- [Collaboratory for Indigenous Data Governance](https://indigenousdatalab.org/)

- [An Indigenous Scientist On Purging Colonialist Practices From Science](https://www.sciencefriday.com/articles/indigenous-science-colonialism/) Excerpt from Fresh Banana Leaves  - Dr Jessica Hernandez on Healing Indigenous Landscapes Through Indigenous Science

- [United Nations Declaration on the Rights of Indigenous Peoples](https://social.desa.un.org/issues/indigenous-peoples/united-nations-declaration-on-the-rights-of-indigenous-peoples)

- [Local Contexts Hub](https://localcontexts.org/) - Grounding Indigenous Rights

- [Laboratory for the Analysis of Social-Ecological Systems in a Globalised world (LASEG)](https://www.laseg.cat/en/projects) - Projects

### Case Studies

- [A global assessment of Indigenous community engagement in climate research](https://iopscience.iop.org/article/10.1088/1748-9326/aaf300/meta) - David-Chavez & Gavin 2018

- [Applying the ‘CARE Principles for Indigenous Data Governance’ to ecology and biodiversity research](https://www.nature.com/articles/s41559-023-02161-2) - Jennings, L., Anderson, T., Martinez, A. et al. Nat Ecol Evol (2023).

## Additional Community Resources 

### Data Commons / Commoning

- [Governing the Commons: The Evolution of Institutions for Collective Action](https://digitalrepository.unm.edu/cgi/viewcontent.cgi?article=1848&context=nrj) - Elinor Ostrom

- [Frontiers of Commoning Podcast](https://david-bollier.simplecast.com/)- "A monthly conversation with creative activists pioneering new forms of commoning." 

- [Open Data Manchester](https://www.opendatamanchester.org.uk/) - Org that hosts both a [Data Cooperatives Working Group](https://www.opendatamanchester.org.uk/data-cooperative-working-group/) and published a co-designed handbook, ["How to Build A Data Cooperative."](https://www.opendatamanchester.org.uk/wp-content/uploads/2024/04/Data-coop-handbook-FINAL-v1_PDF.pdf)

- [Princeton Data Commons Discovery tool](https://datacommons.princeton.edu/discovery/about): Heterogeneous data storage infrastructure with presentation layer that allows researchers to access their data in the way they need with the ability to manage & store behind the scenes 

    - [Background from Dr. Wind Cowles on the FAIR data podcast](https://open.spotify.com/episode/6HspdtkzCnLTuekSlQRMbT) 


- [Global Open Research Commons Interest Group - RDA](https://www.rd-alliance.org/groups/global-open-research-commons-ig)

     - [Background from CJ Woodford on the FAIR data podcast](https://www.rd-alliance.org/groups/global-open-research-commons-ig)

### Data Publishing, Data Standards, & Repository Alignment 

- [OpenTEAM Partnership for Climate-Smart Commodities - Event Storming](https://openteamag.gitlab.io/codesign/pcsc/) - documentation page for data alignment work for USDA-PCSC projects in the OpenTEAM network 

- [Research Data Alliance](https://rd-alliance.org/) - “International, community driven forum for data professionals across the globe to collaborate and develop solutions to enable the open sharing and re-use of data.”

     - [Community of Practice guidelines](https://rd-alliance.org/groups/creating-and-managing-rda-groups/creating-or-joining-community-practice)

     - [Agrisemantics working group](https://rd-alliance.org/groups/agrisemantics-wg.html) - check out their [Semantics of Rice](https://www.rd-alliance.org/system/files/documents/SEMANTICS-RICE_poster_LD.jpg) poster!

     - [Mapping the landscape of digital research tools WG](https://rd-alliance.org/groups/rda-ofr-mapping-landscape-digital-research-tools-wg)

- [Fairsharing Registry](https://fairsharing.org/) - Repository of Standards, databases, & practices 

- [Data Curation Network](https://datacurationnetwork.org/) - Community-led network of curators advancing open research

- [DPGI](https://digitalpublicgoods.net/) - Digital Public Goods Alliance 

- [MAPEO](https://www.digital-democracy.org/mapeo) - “free digital toolset for documenting, monitoring, and mapping many types of data” by Digital Democracy

- [PPOD Ontology](https://github.com/adhollander/ppod) - Data structure for finding relevant people, projects, organizations, and datasets

- [ORCID](https://orcid.org/) - Persistent digital identification 

- [DRYAD](https://datadryad.org/stash) - Community-run data curation & publishing 

- [POSI](https://openscholarlyinfrastructure.org/) - Principles of Open Scholarly Infrastructure

- [HESTIA](https://www.hestia.earth/) - Sustainable-ag focused data schema & platform

- [FAIR Implementation Profile (FIP) Ontology](https://peta-pico.github.io/FAIR-nanopubs/fip/index-en.html#https://w3id.org/fair/fip/terms/FIP-Ontology)

- [Intelligent Cyberinfrastructure with Computational Learning in the Environment (ICICLE)](https://icicle.osu.edu/)


### TRUST Principles

- [Research Data Alliance - TRUST Principles](https://www.rd-alliance.org/rda-community-effort-trust-principles-digital-repositories) Working group location.

- [The TRUST Principles for digital repositories](https://www.nature.com/articles/s41597-020-0486-7) - Original paper published in 2020, outlining Transparency, Responsibility, User focus, Sustainability and Technology as a framework for building trustworthiness. 

- [FAIR-TRUST-CARE Principles for Environmental Data Repositories](https://ui.adsabs.harvard.edu/abs/2021AGUFMIN43B..04O/abstract) 

- [CoreTrustSeal certification](https://www.coretrustseal.org/about/) - Community-based nonprofit offering certification based on [“Core Trustworthy Data Repositories Requirements.”](https://www.coretrustseal.org/why-certification/requirements/)


### Food + Data Projects

- [International Center for Food Ontology Operability Data and Semantics (IC-FOODS)](https://www.ic-foods.org/)

- [NSF Awardee: Developing an Informational Infrastructure for Building Smart Regional Foodsheds](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1737573&HistoricalAwards=false)

- [Tribal Elder Food Box program](https://feedingwi.org/programs/tribalfoodsecurity/) - Feeding Wisconsin 

- [USDA Farm to Institution program](https://www.usda.gov/sites/default/files/documents/6-Farmtoinstitution.pdf)

- [Wisconsin Food Hub Cooperative](https://wifoodhub.com/about-wfhc/)

- [Coalition for Digital Environmental Sustainability](https://www.codes.global/) - “CODES is a global hub for policymakers, academics, technology companies, and NGOs to lead and contribute to digital sustainability initiatives” 

- [USDA Regional Food Systems Partnership program (RFSP)](https://www.ams.usda.gov/services/grants/rfsp)

- [Farm2Facts](https://farm2facts.org/) - Farmers-market data collection toolkit backed by the University of Wisconsin-Madison.

- [INFAS](https://asi.ucdavis.edu/programs/infas) - ‘The Inter-Institutional Network for Food, Agriculture, and Sustainability (INFAS) connects food system scholars, educators, and action-researcher activists across the United States.”

### Other Perspectives

- [AI4OPT](https://www.ai4opt.org/) - Artificial Intelligence Institute for Advances in Optimization

- [DAIR's Possible Futures Blog](https://www.dair-institute.org/blog/) - Distributed AI Research Institute blog series "explor[ing] what the world can look like when we design and deploy technology that centers the needs of our communities."

- [Datasets Have Worldviews](https://pair.withgoogle.com/explorables/dataset-worldviews/) explorable

- [Paolo Freire - Pedagogy of the Oppressed (PDF)](https://envs.ucsc.edu/internships/internship-readings/freire-pedagogy-of-the-oppressed.pdf)
 
